EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "EPP.AMCE.003 - Chargeur NIMH V5"
Date "lundi 06 janvier 2014"
Rev "B"
Comp "UGECAM - CRIP - AMCE"
Comment1 "Mise à jour symboles (Kicad 5)"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R3
U 1 1 52CA8B1F
P 6100 3000
F 0 "R3" H 6000 3000 50  0000 C CNN
F 1 "330" V 6175 3000 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6100 3000 60  0001 C CNN
F 3 "" H 6100 3000 60  0000 C CNN
F 4 "Résistance 0.1W" H 6100 3000 50  0001 C CNN "Désignation"
	1    6100 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 52CA8B31
P 6500 3000
F 0 "R4" H 6400 3000 50  0000 C CNN
F 1 "330" V 6575 3000 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6500 3000 60  0001 C CNN
F 3 "" H 6500 3000 60  0000 C CNN
F 4 "Résistance 0.1W" H 6500 3000 50  0001 C CNN "Désignation"
	1    6500 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 52CA8B3C
P 2900 2200
F 0 "R1" H 2800 2200 50  0000 C CNN
F 1 "1" V 2975 2200 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2900 2200 60  0001 C CNN
F 3 "" H 2900 2200 60  0000 C CNN
F 4 "Résistance 0.1W" H 2900 2200 50  0001 C CNN "Désignation"
	1    2900 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 52CA8B42
P 3550 3150
F 0 "R2" H 3650 3150 50  0000 C CNN
F 1 "390k" V 3625 3150 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3550 3150 60  0001 C CNN
F 3 "" H 3550 3150 60  0000 C CNN
F 4 "Résistance 0.1W" H 3550 3150 50  0001 C CNN "Désignation"
	1    3550 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 52CA8B48
P 7050 4200
F 0 "R7" H 7150 4200 50  0000 C CNN
F 1 "4.22k" V 7125 4200 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7050 4200 60  0001 C CNN
F 3 "" H 7050 4200 60  0000 C CNN
F 4 "Résistance 0.25W" H 7050 4200 50  0001 C CNN "Désignation"
	1    7050 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 52CA8B4E
P 8500 4000
F 0 "R8" H 8400 4000 50  0000 C CNN
F 1 "1" V 8575 4000 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8500 4000 60  0001 C CNN
F 3 "" H 8500 4000 60  0000 C CNN
F 4 "Résistance 0.25W" H 8500 4000 50  0001 C CNN "Désignation"
	1    8500 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 52CA8B54
P 6950 3000
F 0 "R5" H 6850 3000 50  0000 C CNN
F 1 "OPT" H 6825 2925 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6950 3000 60  0001 C CNN
F 3 "" H 6950 3000 60  0000 C CNN
F 4 "Résistance 0.1W" H 6950 3000 50  0001 C CNN "Désignation"
	1    6950 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 52CA8B5A
P 3100 3300
F 0 "R6" H 3000 3300 50  0000 C CNN
F 1 "68k" V 3175 3300 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 3300 60  0001 C CNN
F 3 "" H 3100 3300 60  0000 C CNN
F 4 "Résistance 0.25W" H 3100 3300 50  0001 C CNN "Désignation"
	1    3100 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 52CA8B60
P 1925 4375
F 0 "R11" V 2000 4375 50  0000 C CNN
F 1 "931" V 1825 4375 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1925 4375 60  0001 C CNN
F 3 "" H 1925 4375 60  0000 C CNN
F 4 "Résistance 0.25W" H 1925 4375 50  0001 C CNN "Désignation"
	1    1925 4375
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R12
U 1 1 52CA8B66
P 1925 4625
F 0 "R12" V 2000 4625 50  0000 C CNN
F 1 "2.8k" V 1850 4625 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1925 4625 60  0001 C CNN
F 3 "" H 1925 4625 60  0000 C CNN
F 4 "Résistance 0.25W" H 1925 4625 50  0001 C CNN "Désignation"
	1    1925 4625
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R9
U 1 1 52CA8B6C
P 3200 4250
F 0 "R9" H 3100 4250 50  0000 C CNN
F 1 "576" V 3275 4250 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3200 4250 60  0001 C CNN
F 3 "" H 3200 4250 60  0000 C CNN
F 4 "Résistance 0.25W" H 3200 4250 50  0001 C CNN "Désignation"
	1    3200 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 52CA8B72
P 3200 4750
F 0 "R10" H 3075 4750 50  0000 C CNN
F 1 "2.21k" V 3275 4750 50  0001 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3200 4750 60  0001 C CNN
F 3 "" H 3200 4750 60  0000 C CNN
F 4 "Résistance 0.25W" H 3200 4750 50  0001 C CNN "Désignation"
	1    3200 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 52CA8B92
P 2900 2600
F 0 "C1" H 2775 2700 50  0000 L CNN
F 1 "1µF" H 2975 2525 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2900 2600 60  0001 C CNN
F 3 "" H 2900 2600 60  0000 C CNN
F 4 "Condensateur Céramique" H 2900 2600 50  0001 C CNN "Désignation"
	1    2900 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 52CA8BA4
P 8500 4350
F 0 "C2" H 8525 4450 50  0000 L CNN
F 1 "10µF" H 8575 4275 50  0001 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8500 4350 60  0001 C CNN
F 3 "" H 8500 4350 60  0000 C CNN
F 4 "Condensateur Céramique" H 8500 4350 50  0001 C CNN "Désignation"
	1    8500 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 52CA8BAA
P 1350 2950
F 0 "C3" V 1400 3000 50  0000 L CNN
F 1 "150pF" V 1400 2625 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1350 2950 60  0001 C CNN
F 3 "" H 1350 2950 60  0000 C CNN
F 4 "Condensateur Céramique" H 1350 2950 50  0001 C CNN "Désignation"
	1    1350 2950
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C4
U 1 1 52CA8BB5
P 1350 3150
F 0 "C4" V 1400 3200 50  0000 L CNN
F 1 "470pF" V 1400 2825 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1350 3150 60  0001 C CNN
F 3 "" H 1350 3150 60  0000 C CNN
F 4 "Condensateur Céramique" H 1350 3150 50  0001 C CNN "Désignation"
	1    1350 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C6
U 1 1 52CA8BC1
P 1350 3550
F 0 "C6" V 1400 3600 50  0000 L CNN
F 1 "2700pF" V 1400 3175 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1350 3550 60  0001 C CNN
F 3 "" H 1350 3550 60  0000 C CNN
F 4 "Condensateur Céramique" H 1350 3550 50  0001 C CNN "Désignation"
	1    1350 3550
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C7
U 1 1 52CA8BC7
P 2500 3575
F 0 "C7" H 2375 3675 50  0000 L CNN
F 1 "220pF" H 2550 3475 50  0001 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2500 3575 60  0001 C CNN
F 3 "" H 2500 3575 60  0000 C CNN
F 4 "Condensateur Céramique" H 2500 3575 50  0001 C CNN "Désignation"
	1    2500 3575
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NJFET_GSD Q1
U 1 1 52CAAE9E
P 8550 2600
F 0 "Q1" V 8325 2550 50  0000 C CNN
F 1 "FDN306P" V 8400 2425 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 8550 2600 60  0001 C CNN
F 3 "" H 8550 2600 60  0000 C CNN
F 4 "Transistor MOSFET" H 8550 2600 50  0001 C CNN "Désignation"
	1    8550 2600
	0    1    1    0   
$EndComp
$Comp
L Device:Q_PNP_BCE Q2
U 1 1 52CAAEFC
P 8400 3550
F 0 "Q2" H 8625 3500 50  0000 C CNN
F 1 "2SB1202S" H 8775 3575 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 8400 3550 60  0001 C CNN
F 3 "" H 8400 3550 60  0000 C CNN
F 4 "Transistor PNP" H 8400 3550 50  0001 C CNN "Désignation"
	1    8400 3550
	1    0    0    1   
$EndComp
$Comp
L Device:D_Schottky D1
U 1 1 52CAAF12
P 9150 2450
F 0 "D1" V 9100 2550 40  0000 C CNN
F 1 "B220A" V 9175 2600 40  0000 C CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" H 9150 2450 60  0001 C CNN
F 3 "" H 9150 2450 60  0000 C CNN
F 4 "Diode Schottky" H 9150 2450 50  0001 C CNN "Désignation"
	1    9150 2450
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky D2
U 1 1 52CAAF24
P 8550 2950
F 0 "D2" H 8550 3025 40  0000 C CNN
F 1 "MBRM120LT3" H 8575 2850 40  0000 C CNN
F 2 "Diode_SMD:D_Powermite_KA" H 8550 2950 60  0001 C CNN
F 3 "" H 8550 2950 60  0000 C CNN
F 4 "Diode Schottky" H 8550 2950 50  0001 C CNN "Désignation"
	1    8550 2950
	-1   0    0    1   
$EndComp
$Comp
L Device:Thermistor_NTC TH1
U 1 1 52CABF97
P 8075 5450
F 0 "TH1" H 8225 5525 50  0000 C CNN
F 1 "NTC 10k" H 8300 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8075 5450 60  0001 C CNN
F 3 "" H 8075 5450 60  0000 C CNN
F 4 "Thermistance CTN" H 8075 5450 50  0001 C CNN "Désignation"
	1    8075 5450
	1    0    0    -1  
$EndComp
$Comp
L Chargeur_NIMH_V5-rescue:LTC4060FEFE U1
U 1 1 52CAC227
P 5250 3850
F 0 "U1" H 5300 4750 50  0000 L BNN
F 1 "LTC4060FEFE" H 5300 4650 50  0000 L BNN
F 2 "Package_SO:TSSOP-16-1EP_4.4x5mm_P0.65mm" H 5250 4000 50  0001 C CNN
F 3 "" H 5250 3850 60  0000 C CNN
F 4 "Contrôleur de charge" H 5250 3850 50  0001 C CNN "Désignation"
	1    5250 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:LED LED1
U 1 1 52CACD89
P 6100 2600
F 0 "LED1" V 6000 2700 40  0000 C CNN
F 1 "POWER" V 6200 2475 40  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 2600 60  0001 C CNN
F 3 "" H 6100 2600 60  0000 C CNN
F 4 "LED Verte" H 6100 2600 50  0001 C CNN "Désignation"
	1    6100 2600
	0    1    -1   0   
$EndComp
$Comp
L Device:LED LED2
U 1 1 52CACD9B
P 6500 2600
F 0 "LED2" V 6400 2700 40  0000 C CNN
F 1 "CHARGE" V 6600 2475 40  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6500 2600 60  0001 C CNN
F 3 "" H 6500 2600 60  0000 C CNN
F 4 "LED Rouge" H 6500 2600 50  0001 C CNN "Désignation"
	1    6500 2600
	0    1    -1   0   
$EndComp
$Comp
L Device:Battery BAT1
U 1 1 52CAD0DB
P 8900 4125
F 0 "BAT1" H 9000 4300 50  0000 C CNN
F 1 "2xAA" H 9000 3975 50  0000 C CNN
F 2 "Battery:BatteryHolder_Keystone_2462_2xAA" H 8900 4125 60  0001 C CNN
F 3 "" H 8900 4125 60  0000 C CNN
F 4 "Support LR06" H 8900 4125 50  0001 C CNN "Désignation"
	1    8900 4125
	1    0    0    -1  
$EndComp
Text Label 4450 4400 0    60   ~ 0
1.3V
Text Label 4450 4500 0    60   ~ 0
1.2V
Text Label 4450 4600 0    60   ~ 0
OFF
NoConn ~ 2175 4825
Wire Wire Line
	6000 4450 5800 4450
Wire Wire Line
	5800 4450 5800 4150
Wire Wire Line
	5800 4150 5700 4150
Wire Wire Line
	5700 4050 6400 4050
Wire Wire Line
	6400 4050 6400 4750
Wire Wire Line
	6400 4750 6600 4750
Wire Wire Line
	6000 4350 5900 4350
Wire Wire Line
	5900 4350 5900 4300
Wire Wire Line
	6600 4650 6500 4650
Wire Wire Line
	6500 4650 6500 4550
Wire Wire Line
	6600 4850 6500 4850
Wire Wire Line
	6500 4850 6500 5000
Wire Wire Line
	6000 4550 5900 4550
Wire Wire Line
	5900 4550 5900 4700
Wire Wire Line
	7050 4350 7050 4450
Connection ~ 7050 4450
Wire Wire Line
	8075 5600 8075 5850
Wire Wire Line
	7050 4050 7050 3900
Wire Wire Line
	6850 4450 6850 3950
Wire Wire Line
	6850 3950 5700 3950
Wire Wire Line
	5700 3250 6100 3250
Wire Wire Line
	6100 3250 6100 3150
Wire Wire Line
	6100 2850 6100 2750
Wire Wire Line
	5700 3350 6500 3350
Wire Wire Line
	6500 3350 6500 3150
Wire Wire Line
	6500 2850 6500 2750
Wire Wire Line
	6950 3450 6950 3150
Connection ~ 6950 3450
Wire Wire Line
	7600 3350 7600 3450
Wire Wire Line
	7600 3450 6950 3450
Wire Wire Line
	7600 3800 8500 3800
Wire Wire Line
	7600 3650 7600 3800
Wire Wire Line
	5700 3650 7300 3650
Connection ~ 8500 3800
Wire Wire Line
	8500 4150 8500 4200
Wire Wire Line
	4800 4150 4700 4150
Wire Wire Line
	4700 4150 4700 4400
Connection ~ 4700 4500
Connection ~ 4700 4400
Wire Wire Line
	3450 4750 3450 4600
Wire Wire Line
	3200 4050 3200 4100
Wire Wire Line
	3200 4400 3200 4500
Connection ~ 3200 4500
Connection ~ 3200 4050
Wire Wire Line
	2475 4825 2475 4375
Wire Wire Line
	2475 4375 2075 4375
Wire Wire Line
	2275 4825 2275 4625
Wire Wire Line
	2275 4625 2075 4625
Wire Wire Line
	1775 4625 1675 4625
Wire Wire Line
	1675 4375 1675 4625
Connection ~ 1675 4625
Wire Wire Line
	4800 3250 4700 3250
Wire Wire Line
	4700 3250 4700 2950
Wire Wire Line
	4700 2950 4350 2950
Wire Wire Line
	4350 2850 4450 2850
Wire Wire Line
	4450 2850 4450 2700
Wire Wire Line
	4450 3125 4450 3050
Wire Wire Line
	4450 3050 4350 3050
Wire Wire Line
	2800 3750 4800 3750
Wire Wire Line
	2800 3350 2800 3750
Wire Wire Line
	2400 3350 2500 3350
Wire Wire Line
	2400 3050 2500 3050
Wire Wire Line
	2500 3050 2500 3150
Connection ~ 2500 3350
Wire Wire Line
	2400 3250 2500 3250
Connection ~ 2500 3250
Wire Wire Line
	2400 3150 2500 3150
Connection ~ 2500 3150
Wire Wire Line
	1750 3050 1750 2950
Wire Wire Line
	1750 2950 1500 2950
Wire Wire Line
	1800 3350 1800 3550
Wire Wire Line
	1800 3550 1500 3550
Wire Wire Line
	4800 3400 3550 3400
Wire Wire Line
	3550 3400 3550 3300
Wire Wire Line
	3100 3450 3100 3500
Wire Wire Line
	4800 3500 3400 3500
Wire Wire Line
	3400 3500 3400 3025
Wire Wire Line
	3400 3025 3100 3025
Wire Wire Line
	3100 3025 3100 3150
Wire Wire Line
	3550 3000 3550 2900
Wire Wire Line
	5250 2850 5250 3000
Wire Wire Line
	2900 2850 2900 2750
Wire Wire Line
	2900 1750 2900 1900
Wire Wire Line
	2900 1900 1650 1900
Connection ~ 2900 1900
Wire Wire Line
	1800 2000 1800 2100
Wire Wire Line
	7300 3650 7300 2700
Wire Wire Line
	7300 2700 8250 2700
Connection ~ 7300 3650
Wire Wire Line
	8400 2950 8250 2950
Wire Wire Line
	8250 2950 8250 2700
Connection ~ 8250 2700
Wire Wire Line
	8700 2950 8850 2950
Wire Wire Line
	8850 2950 8850 2700
Wire Wire Line
	8750 2700 8850 2700
Wire Wire Line
	9150 2700 9150 2600
Connection ~ 8850 2700
Connection ~ 9150 2700
Wire Wire Line
	8550 2150 8550 2400
Wire Wire Line
	6100 2150 6500 2150
Wire Wire Line
	9150 2150 9150 2300
Wire Wire Line
	6500 2150 6500 2450
Connection ~ 8550 2150
Wire Wire Line
	6100 2050 6100 2150
Connection ~ 6500 2150
Connection ~ 6100 2150
$Comp
L Device:Battery BAT2
U 1 1 52CAED66
P 9250 4125
F 0 "BAT2" H 9350 4300 50  0000 C CNN
F 1 "2xAAA" H 9375 3975 50  0000 C CNN
F 2 "Battery:BatteryHolder_Keystone_2468_2xAAA" H 9250 4125 60  0001 C CNN
F 3 "" H 9250 4125 60  0000 C CNN
F 4 "Support LR03" H 9250 4125 50  0001 C CNN "Désignation"
	1    9250 4125
	1    0    0    -1  
$EndComp
Connection ~ 8900 3800
Wire Wire Line
	6950 2850 6950 2150
Connection ~ 6950 2150
Wire Wire Line
	1800 2000 1650 2000
Wire Wire Line
	1650 2100 1800 2100
Connection ~ 1800 2100
Connection ~ 9250 3800
Wire Wire Line
	6850 4450 7050 4450
Wire Wire Line
	9900 3000 9800 3000
Wire Wire Line
	9800 3000 9800 3200
Wire Wire Line
	9900 3900 9800 3900
Wire Wire Line
	9800 3900 9800 4550
Text Notes 2125 3075 0    60   ~ 0
A
Text Notes 2125 3175 0    60   ~ 0
B
Text Notes 2125 3275 0    60   ~ 0
C
Text Notes 2125 3375 0    60   ~ 0
D
Text Notes 6875 4875 0    60   ~ 0
A
Text Notes 6875 4700 0    60   ~ 0
B
Text Notes 7725 4800 0    60   ~ 0
EXT
Text Notes 7725 4900 0    60   ~ 0
INT
Text Notes 6275 4400 0    60   ~ 0
B
Text Notes 6275 4575 0    60   ~ 0
A
Text Notes 3875 2900 0    60   ~ 0
NICD
Text Notes 3850 3075 0    60   ~ 0
NIMH
Text Notes 4050 4450 0    60   ~ 0
A
Text Notes 4050 4550 0    60   ~ 0
B
Text Notes 4050 4625 0    60   ~ 0
C
Wire Wire Line
	8075 4900 8075 5300
Wire Wire Line
	7850 5000 7850 5850
Wire Wire Line
	7125 5000 7175 5000
Wire Wire Line
	7125 4800 7125 4900
Wire Wire Line
	7125 4800 7175 4800
Wire Wire Line
	7050 4900 7125 4900
Connection ~ 7125 4900
Text Notes 7725 5000 0    60   ~ 0
OFF
Wire Wire Line
	5250 4650 5250 4450
Wire Wire Line
	7050 4450 7050 4900
Wire Wire Line
	6950 3450 5700 3450
Wire Wire Line
	8500 3800 8500 3850
Wire Wire Line
	8500 3800 8900 3800
Wire Wire Line
	4700 4500 4700 4600
Wire Wire Line
	4700 4400 4700 4500
Wire Wire Line
	3200 4500 3200 4600
Wire Wire Line
	3200 4050 4800 4050
Wire Wire Line
	2500 3350 2800 3350
Wire Wire Line
	2500 3350 2500 3425
Wire Wire Line
	2500 3250 2500 3350
Wire Wire Line
	2500 3150 2500 3250
Wire Wire Line
	2900 1900 2900 2050
Wire Wire Line
	7300 3650 7600 3650
Wire Wire Line
	8250 2700 8350 2700
Wire Wire Line
	8850 2700 9150 2700
Wire Wire Line
	9150 2700 9900 2700
Wire Wire Line
	8550 2150 9150 2150
Wire Wire Line
	6500 2150 6950 2150
Wire Wire Line
	6100 2150 6100 2450
Wire Wire Line
	8900 3800 9250 3800
Wire Wire Line
	6950 2150 8550 2150
Wire Wire Line
	1800 2100 1800 2350
Wire Wire Line
	9250 3800 9900 3800
Wire Wire Line
	7125 4900 7125 5000
Wire Wire Line
	7125 4900 7175 4900
$Comp
L power:VCC #PWR0101
U 1 1 5D7EC979
P 2900 1750
F 0 "#PWR0101" H 2900 1600 50  0001 C CNN
F 1 "VCC" H 2917 1923 50  0000 C CNN
F 2 "" H 2900 1750 50  0001 C CNN
F 3 "" H 2900 1750 50  0001 C CNN
	1    2900 1750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5D7ED851
P 6100 2050
F 0 "#PWR0102" H 6100 1900 50  0001 C CNN
F 1 "VCC" H 6117 2223 50  0000 C CNN
F 2 "" H 6100 2050 50  0001 C CNN
F 3 "" H 6100 2050 50  0001 C CNN
	1    6100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0103
U 1 1 5D7EE12A
P 4450 2700
F 0 "#PWR0103" H 4450 2550 50  0001 C CNN
F 1 "VCC" H 4467 2873 50  0000 C CNN
F 2 "" H 4450 2700 50  0001 C CNN
F 3 "" H 4450 2700 50  0001 C CNN
	1    4450 2700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0104
U 1 1 5D7EEB93
P 5250 2850
F 0 "#PWR0104" H 5250 2700 50  0001 C CNN
F 1 "VCC" H 5267 3023 50  0000 C CNN
F 2 "" H 5250 2850 50  0001 C CNN
F 3 "" H 5250 2850 50  0001 C CNN
	1    5250 2850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0105
U 1 1 5D7EF0F1
P 3550 2900
F 0 "#PWR0105" H 3550 2750 50  0001 C CNN
F 1 "VCC" H 3567 3073 50  0000 C CNN
F 2 "" H 3550 2900 50  0001 C CNN
F 3 "" H 3550 2900 50  0001 C CNN
	1    3550 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even JP5
U 1 1 5D7EF9D0
P 2100 3150
F 0 "JP5" H 2150 3350 50  0000 C CNN
F 1 "TIMER" H 2150 2850 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 2100 3150 50  0001 C CNN
F 3 "~" H 2100 3150 50  0001 C CNN
F 4 "Embase CI" H 2100 3150 50  0001 C CNN "Désignation"
	1    2100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 3050 1900 3050
Wire Wire Line
	1800 3350 1900 3350
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JP7
U 1 1 5D81CFA5
P 4025 4500
F 0 "JP7" H 4075 4725 50  0000 C CNN
F 1 "RECHARGE" H 4075 4275 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4025 4500 50  0001 C CNN
F 3 "~" H 4025 4500 50  0001 C CNN
F 4 "Embase CI" H 4025 4500 50  0001 C CNN "Désignation"
	1    4025 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 4825 2375 4050
Wire Wire Line
	2375 4050 3200 4050
Wire Wire Line
	4325 4400 4700 4400
Wire Wire Line
	4325 4500 4700 4500
Wire Wire Line
	4325 4600 4700 4600
Wire Wire Line
	3650 4400 3825 4400
Wire Wire Line
	3200 4500 3825 4500
Wire Wire Line
	3450 4600 3825 4600
$Comp
L power:VCC #PWR0106
U 1 1 5D897561
P 3650 4300
F 0 "#PWR0106" H 3650 4150 50  0001 C CNN
F 1 "VCC" H 3667 4473 50  0000 C CNN
F 2 "" H 3650 4300 50  0001 C CNN
F 3 "" H 3650 4300 50  0001 C CNN
	1    3650 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4400 3650 4300
$Comp
L power:GND #PWR0107
U 1 1 5D8A1446
P 1800 2350
F 0 "#PWR0107" H 1800 2100 50  0001 C CNN
F 1 "GND" H 1805 2177 50  0000 C CNN
F 2 "" H 1800 2350 50  0001 C CNN
F 3 "" H 1800 2350 50  0001 C CNN
	1    1800 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5D8A1C4F
P 2900 2850
F 0 "#PWR0108" H 2900 2600 50  0001 C CNN
F 1 "GND" H 2905 2677 50  0000 C CNN
F 2 "" H 2900 2850 50  0001 C CNN
F 3 "" H 2900 2850 50  0001 C CNN
	1    2900 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5D8A1FED
P 2500 3800
F 0 "#PWR0109" H 2500 3550 50  0001 C CNN
F 1 "GND" H 2505 3627 50  0000 C CNN
F 2 "" H 2500 3800 50  0001 C CNN
F 3 "" H 2500 3800 50  0001 C CNN
	1    2500 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5D8A241D
P 1125 3650
F 0 "#PWR0110" H 1125 3400 50  0001 C CNN
F 1 "GND" H 1130 3477 50  0000 C CNN
F 2 "" H 1125 3650 50  0001 C CNN
F 3 "" H 1125 3650 50  0001 C CNN
	1    1125 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5D8A27C2
P 3100 3500
F 0 "#PWR0111" H 3100 3250 50  0001 C CNN
F 1 "GND" H 3105 3327 50  0000 C CNN
F 2 "" H 3100 3500 50  0001 C CNN
F 3 "" H 3100 3500 50  0001 C CNN
	1    3100 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5D8CB4F5
P 4450 3125
F 0 "#PWR0112" H 4450 2875 50  0001 C CNN
F 1 "GND" H 4455 2952 50  0000 C CNN
F 2 "" H 4450 3125 50  0001 C CNN
F 3 "" H 4450 3125 50  0001 C CNN
	1    4450 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 4625 1675 4825
$Comp
L power:GND #PWR0113
U 1 1 5D8D4CF6
P 1675 4825
F 0 "#PWR0113" H 1675 4575 50  0001 C CNN
F 1 "GND" H 1680 4652 50  0000 C CNN
F 2 "" H 1675 4825 50  0001 C CNN
F 3 "" H 1675 4825 50  0001 C CNN
	1    1675 4825
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4900 3200 4950
$Comp
L power:GND #PWR0114
U 1 1 5D8D5A8E
P 3200 4950
F 0 "#PWR0114" H 3200 4700 50  0001 C CNN
F 1 "GND" H 3205 4777 50  0000 C CNN
F 2 "" H 3200 4950 50  0001 C CNN
F 3 "" H 3200 4950 50  0001 C CNN
	1    3200 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5D8D5F74
P 3450 4750
F 0 "#PWR0115" H 3450 4500 50  0001 C CNN
F 1 "GND" H 3455 4577 50  0000 C CNN
F 2 "" H 3450 4750 50  0001 C CNN
F 3 "" H 3450 4750 50  0001 C CNN
	1    3450 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5D8D62AA
P 5250 4650
F 0 "#PWR0116" H 5250 4400 50  0001 C CNN
F 1 "GND" H 5255 4477 50  0000 C CNN
F 2 "" H 5250 4650 50  0001 C CNN
F 3 "" H 5250 4650 50  0001 C CNN
	1    5250 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5D8D65E8
P 5900 4700
F 0 "#PWR0117" H 5900 4450 50  0001 C CNN
F 1 "GND" H 5905 4527 50  0000 C CNN
F 2 "" H 5900 4700 50  0001 C CNN
F 3 "" H 5900 4700 50  0001 C CNN
	1    5900 4700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0118
U 1 1 5D8DE6CD
P 5900 4300
F 0 "#PWR0118" H 5900 4150 50  0001 C CNN
F 1 "VCC" H 5917 4473 50  0000 C CNN
F 2 "" H 5900 4300 50  0001 C CNN
F 3 "" H 5900 4300 50  0001 C CNN
	1    5900 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5D8DEC51
P 6500 5000
F 0 "#PWR0119" H 6500 4750 50  0001 C CNN
F 1 "GND" H 6505 4827 50  0000 C CNN
F 2 "" H 6500 5000 50  0001 C CNN
F 3 "" H 6500 5000 50  0001 C CNN
	1    6500 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5D8DEFCA
P 7850 5850
F 0 "#PWR0120" H 7850 5600 50  0001 C CNN
F 1 "GND" H 7855 5677 50  0000 C CNN
F 2 "" H 7850 5850 50  0001 C CNN
F 3 "" H 7850 5850 50  0001 C CNN
	1    7850 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5D8DF523
P 8075 5850
F 0 "#PWR0121" H 8075 5600 50  0001 C CNN
F 1 "GND" H 8080 5677 50  0000 C CNN
F 2 "" H 8075 5850 50  0001 C CNN
F 3 "" H 8075 5850 50  0001 C CNN
	1    8075 5850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0122
U 1 1 5D8DF749
P 6500 4550
F 0 "#PWR0122" H 6500 4400 50  0001 C CNN
F 1 "VCC" H 6517 4723 50  0000 C CNN
F 2 "" H 6500 4550 50  0001 C CNN
F 3 "" H 6500 4550 50  0001 C CNN
	1    6500 4550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0123
U 1 1 5D8DFD51
P 7050 3900
F 0 "#PWR0123" H 7050 3750 50  0001 C CNN
F 1 "VCC" H 7067 4073 50  0000 C CNN
F 2 "" H 7050 3900 50  0001 C CNN
F 3 "" H 7050 3900 50  0001 C CNN
	1    7050 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5D8E01D4
P 8500 4550
F 0 "#PWR0124" H 8500 4300 50  0001 C CNN
F 1 "GND" H 8505 4377 50  0000 C CNN
F 2 "" H 8500 4550 50  0001 C CNN
F 3 "" H 8500 4550 50  0001 C CNN
	1    8500 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5D8E0505
P 8900 4550
F 0 "#PWR0125" H 8900 4300 50  0001 C CNN
F 1 "GND" H 8905 4377 50  0000 C CNN
F 2 "" H 8900 4550 50  0001 C CNN
F 3 "" H 8900 4550 50  0001 C CNN
	1    8900 4550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JP4
U 1 1 5D8E082E
P 7375 4900
F 0 "JP4" H 7425 5125 50  0000 C CNN
F 1 "NTC" H 7425 4675 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 7375 4900 50  0001 C CNN
F 3 "~" H 7375 4900 50  0001 C CNN
F 4 "Embase CI" H 7375 4900 50  0001 C CNN "Désignation"
	1    7375 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8075 4800 8075 4575
Wire Wire Line
	7675 4800 8075 4800
Wire Wire Line
	7675 4900 8075 4900
Wire Wire Line
	7675 5000 7850 5000
$Comp
L power:GND #PWR0126
U 1 1 5D974530
P 9250 4550
F 0 "#PWR0126" H 9250 4300 50  0001 C CNN
F 1 "GND" H 9255 4377 50  0000 C CNN
F 2 "" H 9250 4550 50  0001 C CNN
F 3 "" H 9250 4550 50  0001 C CNN
	1    9250 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 5D974A30
P 9800 4550
F 0 "#PWR0127" H 9800 4300 50  0001 C CNN
F 1 "GND" H 9805 4377 50  0000 C CNN
F 2 "" H 9800 4550 50  0001 C CNN
F 3 "" H 9800 4550 50  0001 C CNN
	1    9800 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5D974D4C
P 9800 3200
F 0 "#PWR0128" H 9800 2950 50  0001 C CNN
F 1 "GND" H 9805 3027 50  0000 C CNN
F 2 "" H 9800 3200 50  0001 C CNN
F 3 "" H 9800 3200 50  0001 C CNN
	1    9800 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 JP8
U 1 1 5D975A0E
P 1450 2000
F 0 "JP8" H 1450 1800 50  0000 C CNN
F 1 "JACK ALIM 2.1MM" H 1450 2225 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 1450 2000 50  0001 C CNN
F 3 "~" H 1450 2000 50  0001 C CNN
F 4 "Embase Secteur Jacl" H 1450 2000 50  0001 C CNN "Désignation"
	1    1450 2000
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 JP1
U 1 1 5D97693A
P 4150 2950
F 0 "JP1" H 4150 2750 50  0000 C CNN
F 1 "CHEM" H 4150 3175 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4150 2950 50  0001 C CNN
F 3 "~" H 4150 2950 50  0001 C CNN
F 4 "Embase CI" H 4150 2950 50  0001 C CNN "Désignation"
	1    4150 2950
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 JP2
U 1 1 5D977D54
P 6200 4450
F 0 "JP2" H 6200 4650 50  0000 C CNN
F 1 "SEL0" H 6200 4250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6200 4450 50  0001 C CNN
F 3 "~" H 6200 4450 50  0001 C CNN
F 4 "Embase CI" H 6200 4450 50  0001 C CNN "Désignation"
	1    6200 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 JP3
U 1 1 5D97AB70
P 6800 4750
F 0 "JP3" H 6800 4950 50  0000 C CNN
F 1 "SEL1" H 6800 4550 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6800 4750 50  0001 C CNN
F 3 "~" H 6800 4750 50  0001 C CNN
F 4 "Embase CI" H 6800 4750 50  0001 C CNN "Désignation"
	1    6800 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 JP9
U 1 1 5D97BD7B
P 10100 3800
F 0 "JP9" H 10180 3792 50  0000 L CNN
F 1 "EXT_BAT" H 10180 3701 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 10100 3800 50  0001 C CNN
F 3 "~" H 10100 3800 50  0001 C CNN
F 4 "Bornier à vis" H 10100 3800 50  0001 C CNN "Désignation"
	1    10100 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 E2
U 1 1 5D97C706
P 10100 3000
F 0 "E2" H 10180 3042 50  0000 L CNN
F 1 "GND" H 10180 2951 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 10100 3000 50  0001 C CNN
F 3 "~" H 10100 3000 50  0001 C CNN
F 4 "Plot à fourches" H 10100 3000 50  0001 C CNN "Désignation"
	1    10100 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 E1
U 1 1 5D97D88F
P 10100 2700
F 0 "E1" H 10180 2742 50  0000 L CNN
F 1 "SYST LOAD" H 10180 2651 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 10100 2700 50  0001 C CNN
F 3 "~" H 10100 2700 50  0001 C CNN
F 4 "Plot à fourches" H 10100 2700 50  0001 C CNN "Désignation"
	1    10100 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 E3
U 1 1 5D97E114
P 8075 4375
F 0 "E3" V 8250 4450 50  0000 R CNN
F 1 "EXT_NTC" V 8175 4450 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 8075 4375 50  0001 C CNN
F 3 "~" H 8075 4375 50  0001 C CNN
F 4 "Plot à fourches" H 8075 4375 50  0001 C CNN "Désignation"
	1    8075 4375
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 JP6
U 1 1 5D97FE3A
P 2375 5025
F 0 "JP6" V 2375 5200 50  0000 L CNN
F 1 "CHARGE CURRENT" V 2500 4625 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2375 5025 50  0001 C CNN
F 3 "~" H 2375 5025 50  0001 C CNN
F 4 "Embase CI" H 2375 5025 50  0001 C CNN "Désignation"
	1    2375 5025
	0    1    1    0   
$EndComp
Wire Wire Line
	1775 4375 1675 4375
Wire Wire Line
	2900 2350 2900 2450
Wire Wire Line
	2500 3725 2500 3800
Wire Wire Line
	8500 4500 8500 4550
$Comp
L Device:C C5
U 1 1 52CA8BBB
P 1350 3350
F 0 "C5" V 1400 3400 50  0000 L CNN
F 1 "1200pF" V 1400 2975 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1350 3350 60  0001 C CNN
F 3 "" H 1350 3350 60  0000 C CNN
F 4 "Condensateur Céramique" H 1350 3350 50  0001 C CNN "Désignation"
	1    1350 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1500 3350 1750 3350
Wire Wire Line
	1750 3350 1750 3250
Wire Wire Line
	1750 3250 1900 3250
Wire Wire Line
	1500 3150 1900 3150
Wire Wire Line
	1200 2950 1125 2950
Wire Wire Line
	1125 2950 1125 3150
Wire Wire Line
	1200 3550 1125 3550
Connection ~ 1125 3550
Wire Wire Line
	1125 3550 1125 3650
Wire Wire Line
	1200 3350 1125 3350
Connection ~ 1125 3350
Wire Wire Line
	1125 3350 1125 3550
Wire Wire Line
	1200 3150 1125 3150
Connection ~ 1125 3150
Wire Wire Line
	1125 3150 1125 3350
Wire Wire Line
	8900 3800 8900 3925
Wire Wire Line
	9250 3800 9250 3925
Wire Wire Line
	9250 4325 9250 4550
Wire Wire Line
	8900 4325 8900 4550
Wire Wire Line
	8500 3800 8500 3750
Wire Wire Line
	8500 3350 7600 3350
Wire Wire Line
	5700 3550 8200 3550
$EndSCHEMATC
